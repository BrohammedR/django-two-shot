from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceiptForm, ExpenseCategoryForm, CreateAcctForm
# Create your views here.


@login_required
def receipt_list(request):
    receipt_list= Receipt.objects.filter(purchaser=request.user)
    context={
        "receipt_list": receipt_list
    }
    return render(request, 'receipts/receiptlist.html', context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receiptform = form.save(commit=False)
            receiptform.purchaser = request.user
            receiptform.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {
        'form': form,
    }
    return render(request, 'receipts/create.html', context)

@login_required
def expense_category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    receipts= Receipt.objects.filter(purchaser=request.user)
    context = {
        'categories': categories,
        "receipts": receipts
    }
    return render(request, 'receipts/categories/categorylist.html', context)

@login_required
def account_list(request):
    accounts=Account.objects.filter(owner=request.user)
    context={
        'accounts':accounts
    }
    return render(request, 'receipts/accounts/accountlist.html', context)

@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()
    context={
        "form":form
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAcctForm(request.POST)
        if form.is_valid():
            account=form.save(commit=False)
            account.owner= request.user
            form.save()
            return redirect('account_list')
    else:
        form = CreateAcctForm()
    context={
        'form':form,
    }
    return render(request, "receipts/create_acct.html", context)
