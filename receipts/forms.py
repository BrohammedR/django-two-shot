from django import forms
from receipts.models import Receipt, ExpenseCategory, Account

class CreateReceiptForm(forms.ModelForm):
    class Meta:
        model= Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
            ]


class CreateAcctForm(forms.ModelForm):
    class Meta:
        model=Account
        fields=[
            "name",
            "number",
        ]
